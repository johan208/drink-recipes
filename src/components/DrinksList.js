import React, {useContext} from 'react';
import { RecipesContext } from '../context/RecipesContext';
import Drink from './Drink';

const RecipeList = () => {
    const {recipes} = useContext(RecipesContext)

    return ( 
        <div className="container">
            <div className="row mt-5">
                {recipes.map(recipe => (
                    <Drink 
                    key={recipe.idDrink}
                    recipe={recipe} />
                ))}
            </div>
        </div>
        
     );
}
 
export default RecipeList;