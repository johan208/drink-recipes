import React, {createContext, useState, useEffect} from 'react';
import Axios from 'axios';


export const modalContext = createContext();


const ModalProvider = (props) => {
    
    //state del provider
    const [idDrink,setIdDrink] = useState(null);
    const [drinkDetails,setDrinkDetails] = useState({});
    useEffect( () => {
        if(!idDrink) return ;

        const getDrinkDetails = async () =>{
            const url = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${idDrink}`
            const result = await Axios.get(url);
            setDrinkDetails(result.data.drinks[0]);

        }
        getDrinkDetails();
    },[idDrink])
    return ( 
        <modalContext.Provider
        value={{
            setIdDrink,
            setDrinkDetails,
            drinkDetails
            
        }}
        >
            {props.children}
        </modalContext.Provider>

     );
}
export default ModalProvider;
