import React from 'react';


const Header = () => {
    return ( 
        <header className="">
            <div className="headerBackground">
                <div className="tittleWrapper">
                    <h1 >Lahoo Drinks</h1>
                    <p className="subtitle1" >Search drinks by ingredient, category or drink type</p>
                </div>
                
            </div> 
        </header>
     );
}
 
export default Header;