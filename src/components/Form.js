import React, {useContext, useState} from 'react';
import { CategoriesContext } from './../context/CategoriesContext';
import { RecipesContext } from './../context/RecipesContext';

import Button from '@material-ui/core/Button';

const Form = () => {

    const {categories,ingredients,drinktype} = useContext(CategoriesContext);
    const {searchRecipes,setConsult} = useContext(RecipesContext);

    const [search,setSearch] = useState({
        name:'',
        category:'',
        type:''
    })

    const getDataSearch = e => {
        setSearch({
            ...search,
            [e.target.name] : e.target.value
        })
    }

    return ( 
        <form className="col-12"
        onSubmit={e => {
            e.preventDefault();
            searchRecipes(search);
            setConsult(true);
        }}>
            <fieldset>
                <legend className="text-center">
                    <p className="subtitle2" ><i>just use 1 filter at time</i></p>
                </legend>
            </fieldset>

            <div className="row">
            <div className="col-md-3 mt-3">
                    <select 
                    className="form-control"
                    name="name"
                    onChange={getDataSearch}
                    >
                        <option value="">--Select Ingredient --</option>
                        {ingredients.map(ingredient =>(
                            <option key={ingredient.strIngredient1} value={ingredient.strIngredient1}>
                                {ingredient.strIngredient1}
                            </option>
                        ))}
                    </select>
                </div>

                <div className="col-md-3 mt-3">
                    <select 
                    className="form-control"
                    name="category"
                    onChange={getDataSearch}
                    >
                        <option value="">--Select category --</option>
                        {categories.map(category =>(
                            <option key={category.strCategory} value={category.strCategory}>
                                {category.strCategory}
                            </option>

                        ))}
                    </select>
                </div>
                <div className="col-md-3 mt-3">
                    <select 
                    className="form-control"
                    name="type"
                    onChange={getDataSearch}
                    >
                        <option value="">--Select Type --</option>
                        {drinktype.map(type =>(
                            <option key={type.strAlcoholic} value={type.strAlcoholic}>
                                {type.strAlcoholic}
                            </option>

                        ))}
                    </select>
                </div>
                <div className="col-md-3 mt-3">
                    <Button 
                    type="submit"
                    variant="contained"
                    fullWidth
                    color="primary" >
                    Search
                    </Button>
                </div>
            </div>
        </form>
     );
}
 
export default Form;
