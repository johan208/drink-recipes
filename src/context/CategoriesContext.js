import React, {createContext,useState,useEffect} from 'react';
import Axios from 'axios';

//crear context
export const CategoriesContext = createContext();

//crear provider donde se encuentran las funciones y el state
const CategoriesProvider = (props) => {

    const [categories,setcategories] = useState([]);
    const [ingredients,setIngredients] = useState([]);
    const [drinktype,setDrinkType] = useState([]);

    useEffect( () => {
        const getCategories = async () => {
            const url = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list';
            const categories = await Axios.get(url);
            setcategories(categories.data.drinks)
        }
        const getIngredients = async () => {
            const url = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list';
            const ingredients = await Axios.get(url);
            setIngredients(ingredients.data.drinks)
        }
        const getDrinkType = async () => {
            const url = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?a=list';
            const type = await Axios.get(url);
            setDrinkType(type.data.drinks)
            
        }
        getDrinkType();
        getIngredients();
        getCategories();
    },[])
    return(
        <CategoriesContext.Provider
        value={{
            categories,
            ingredients,
            drinktype
        }}>
            {props.children}
        </CategoriesContext.Provider>
    )
}

export default CategoriesProvider;