import React, {createContext, useState, useEffect} from 'react';
import Axios from 'axios';


export const RecipesContext = createContext();


const RecipesProvider = (props) => {
    const [recipes,setRecipes] = useState([])
    const [search,searchRecipes] = useState({
        name:'',
        category:'',
        type:''
    })
    const [consult,setConsult] = useState(false);

    const {name,category,type} = search;
    useEffect( () => {
        if(consult) {
            if (name === '' && category === '' && type !== '') {
                const getRecipes = async () => {

                    let cutedType = type.replace(/ /g, "_");
                    const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${cutedType}`;
                    const result = await Axios.get(url);
                    console.log(result.config.url);
                    setRecipes(result.data.drinks);    
            }
            getRecipes();
            } else if (name === '' && category !== '' && type === '') {
                const getRecipes = async () => {
                    let cutedCategory = category.replace(/ /g, "_");
                    const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=${cutedCategory}`;
                    const result = await Axios.get(url);
                    console.log(result.config.url);
                    setRecipes(result.data.drinks);    
            }
            getRecipes();
            } else if (name !== '' && category === '' && type === '') {
                const getRecipes = async () => {
                    let cutedName= name.replace(/ /g, "_");
                    const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${cutedName}`;
                    const result = await Axios.get(url);
                    console.log(result.config.url);
                    setRecipes(result.data.drinks);    
            }
            getRecipes();
            }
            
        }
    }, [search])


    return ( 
        <RecipesContext.Provider
        value={{
            recipes,
            searchRecipes,
            setConsult
        }}>
            {props.children}
        </RecipesContext.Provider>
     );
}
 
export default RecipesProvider;