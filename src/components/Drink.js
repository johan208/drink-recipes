import React,{useContext,useState} from 'react';
import { modalContext } from './../context/ModalContext';

import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      textDecoration: 'none',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      width: 450,
      maxHeight: '95vh',
      overflowY: 'auto',
      border:'none',


      
    },
  }));
  

const Drink = ({recipe}) => {
    //conf modal
    const classes = useStyles();
    const [open,setOpen] =useState(false);

    const handleOpen = () => {
        setOpen(true);
      };
    
      const handleClose = () => {
        setOpen(false);
      };

    //muestra y formatea ingredientes
    const showIngredients = details => {
        let ingredients = [];
        for(let i=1;i<16;i++) {
            if(details[`strIngredient${i}`]) {
                ingredients.push(
                <li>{details[`strIngredient${i}`]} - {details[`strMeasure${i}`]} </li>
                )
            }
        }
        return ingredients;
    }

    const {setIdDrink,drinkDetails,setDrinkDetails} = useContext(modalContext);

    return ( 
        <div className="col-sm-4 mb-5">
            <div className="card cardHeight">
                <div className="card-header">
                    <h3 className="card-title text-center">{recipe.strDrink}</h3>
                </div>
                
                <img className="card-img-top" src={recipe.strDrinkThumb} alt={`${recipe.strDrink} thumb`}/>
                <div className="card-body"> 
                    
                    <Button 
                    onClick={() => {
                        setIdDrink(recipe.idDrink)
                        handleOpen();
                    }}
                    variant="contained"
                    color="primary"
                    fullWidth>
                        Datails
                    </Button> 
                    <Modal
                    aria-labelledby="Drinks"
                    aria-describedby="Drinks details"
                    className={classes.modal}
                    open={open}
                    onClose={() => {
                        handleClose();
                        setIdDrink(null);
                        setDrinkDetails({});
                    }}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                    timeout: 500,
                    }}>
                        <Fade in={open}>
                            <div className={classes.paper}>
                                    <h3 className="text-center" style={{textTransform:'uppercase'}}>{drinkDetails.strDrink}</h3>
                                    <h5 className="mt-2 text-center">Instructions to prepare</h5>

                                    <p>
                                        {drinkDetails.strInstructions}
                                    </p>
                                    <img className="img-fluid" src={drinkDetails.strDrinkThumb} alt={`${drinkDetails.strDrink} thumb`}/>
                                    <h5 className="text-center mt-5">Ingredients and Measure</h5>
                                    <div className="container mt-3" style={{display:'flex',justifyContent:'center'}}>
                                        <ul>
                                            {showIngredients(drinkDetails)}
                                        </ul>
                                    </div>
                                    
                            </div>
                        </Fade>
                    </Modal>
                </div>
            </div>
        </div>
     );
}
 
export default Drink;